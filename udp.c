#include "net.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <sys/time.h>
#include <sys/socket.h>
#include <netpacket/packet.h>
#include <net/if.h>
#include <net/ethernet.h> /* the l2 protocols */
#include <arpa/inet.h>
#include <fcntl.h>

#include <sys/ioctl.h>
#include <pthread.h>

char if_name[IF_NAMESIZE];
int if_mtu;
int if_index;

/**
 * Tramas
 */
u_char sentframe[128][64];
u_char recvframe[128][64];

u_char mac_broadcast[6] = 				{0xff,0xff,0xff,0xff,0xff,0xff};
u_char ip_source[4];
u_char mask[4];

/**
 * Protocolo Ethernet
 */
u_char mac_source[6];
u_char mac_target[6];
u_char eth_type[2] = 					{0x08,0x00};

/**
 * Protocolo IP
 */
u_char version_ihl[1] = 				{0x45};
u_char ip_type[1] = 					{0x00};
u_char len[2] = 						{0x00, 0x2c};
u_char ip_id[2] = 						{0x09, 0x0d};
u_char flags_offset[2] = 				{0x00, 0x00};
u_char ttl[1] = 						{0x80};
u_char protocol[1] = 					{0x11};
u_char ip_checksum[2] = 				{0x00, 0x00};
u_char ip_target[4] = 					{0x00, 0x00, 0x00, 0x00};

/**
 * Protocolo UDP
 */
u_char po[2] = 							{0x00,0x00};
u_char pd[2]= 							{0x00,0x00};
u_char udp_len[2]= 						{0x00,0x28};
u_char udp_checksum[2]= 				{0x00,0x00};

int packet_socket[128];
int ip_cksum;

pthread_t udp_t[128];
pthread_t sleep_t[128];

void *t_sleep (void *arg)
{
	usleep(1000000);
	pthread_cancel (udp_t[(int)arg]);
	pthread_exit(0);
}

/* Calculo del checksum */
u_short cksum (u_char *addr, int len){
	register int sum = 0;
	u_short answer = 0;
	u_short *wp;

	for (wp = (u_short*)addr; len > 1; wp++, len -= 2)
		sum += *wp;

	if (len == 1){
		*(u_char *)&answer = *(u_char *)wp;
		sum += answer;
	}

	sum = (sum >> 16) + (sum & 0xffff);

	sum += (sum >> 16);

	answer = ~sum;
	return answer;
}

void *t_udp (void *arg)
{
	int n = (int)arg*512;
	int k, udp_cksum, numbytes;

	for (k = n; k < 512+n; k++)
	{
		memcpy(pd, &k, 2);
		udp_cksum = cksum(sentframe[(int)arg]+24, 8);

		memcpy(udp_checksum, &udp_cksum, 2);
		memcpy(sentframe[(int)arg]+40, udp_checksum, 2);

		send_frame(packet_socket[(int)arg], sentframe[(int)arg], if_index, 64);

		printf("Puerto %d\n", k);

		while (1){

			if((numbytes = recvfrom(packet_socket[(int)arg], recvframe[(int)arg], 1514, 0, NULL, 0)) == -1)
				;

			else if (!memcmp(recvframe+0, mac_source, 6) && !memcmp(recvframe+12, eth_type, 2) && !memcmp (recvframe+26, ip_target, 4))
			{
				if(!memcmp(recvframe+34, pd, 2) && !memcmp (recvframe+36, po, 2) )
				{
					pthread_cancel (sleep_t[(int)arg]);
					printf("\t\tOK\n");
					pthread_exit(0);
				}
			}
		}
	}

}

int main(int argc, char const *argv[])
{
	int i;
	/**
	 * Checamos argumentos de ejecución
	 */
	if (argc != 2){
		printf("Port Scanner UDP\nUso: sudo ./a.out [ip]");
		return EXIT_FAILURE;
	}

	for (i = 0; i < 128; i++)
	{
		/**
		 * Creamos el socket crudo
		 */
		packet_socket[i] = socket (AF_PACKET, SOCK_RAW, htons (ETH_P_ALL));

		/**
		 * Checamos errores
		 */
		if (packet_socket[i] < 0)
			perror ("\nError al abrir socket\n");
		
		/**
		 * Establecemos banderas para socket no bloqueante
		 */
		if (fcntl(packet_socket[i] , F_SETFL, fcntl(packet_socket[i], F_GETFL) | O_NONBLOCK) != 0)
			perror ("fcntl()");
	}

	get_gatewaymac(mac_target);

	/**
	 * Obtenemos la informacion de la interfaz y de la terminal fuente
	 */
	if_index = get_info(packet_socket[0], if_name, &if_mtu, mac_source, ip_source, mask);

	getip_url((u_char *)argv[1], ip_target);

	for (i = 0; i < 128; i++)
	{
		/**
		 * Establecemos las cabeceras necesarias a la trama
		 */
		set_eth_header(sentframe[i], mac_target, mac_source, eth_type);
		set_ip_header (sentframe[i], version_ihl, ip_type, len, ip_id, flags_offset, ttl, protocol, ip_checksum, ip_source, ip_target);

		ip_cksum = cksum(sentframe[i]+14, 20);

		memcpy(ip_checksum, &ip_cksum, 2);
		memcpy(sentframe[i]+24, ip_checksum, 2);

		set_udp_header(sentframe[i], po, pd, udp_len, udp_checksum);

		pthread_create(&(udp_t[i]), NULL, t_udp, (void *)&i);
		pthread_create(&(sleep_t[i]), NULL, t_sleep, (void *)&i);
	}

	return 0;
}