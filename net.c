/**
 * NET.C
 *
 * Juan Carlos Navarrete Gordillo
 * Escuela Superior de Cómputo, IPN
 *
 * Curso: Redes de Computadoras
 * Prof. Gilberto Sánchez Quintanilla
 *
 * Implementacion de Libreria de funciones útiles para el curso
 * Compilacion: gcc -static -c net.c -o net.o && ar -rcs libnet.a net.o
 */

/**
 * Librerias Incluidas
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <sys/socket.h>
#include <netpacket/packet.h>
#include <net/if.h>
#include <net/ethernet.h> /* the l2 protocols */
#include <arpa/inet.h>
#include <netdb.h>
#include <fcntl.h>

#include <sys/ioctl.h>

#include "mysql/mysql.h" /* libreria que nos permite hacer el uso de las conexiones y consultas con MySQL */
#include "net.h"

/**
 * Implementación de Funciones
 */

/**
 * Guarda una direccion IP para enviar en una trama a partir de una cadena ingresada por el usuario
 * @param arg [Cadena ingresada por el usuario]
 * @param ip  [Cadena en donde se guardará la Direccion IP]
 */
void get_ip(const char *arg, u_char *ip) {
	sscanf(arg, "%d.%d.%d.%d", (int *)&ip[0], (int *)&ip[1], (int *)&ip[2], (int *)&ip[3]);
}

/**
 * Obtiene la direccion IP de la red en la que se encuentra la terminal
 * @param ip   [IP fuente]
 * @param mask [Máscara de subred]
 * @param net  [Direccion destino]
 */
void get_net(u_char *ip, u_char *mask, u_char *net) {
	int i;
	for (i = 0; i < 4; i++) {
		net[i] = ip[i] & mask[i];
	}
}

/**
 * Imprime en la salida estándar una cadena en formato de trama
 * @param tam   [tamaño de la trama a imprimir]
 * @param trama [cadena a imprimir]
 */
void print_frame(int tam, u_char *trama) {
	int i;
	for (i = 0; i < tam; i++) {
		if (i % 8 == 0) printf("  ");
		if (i % 16 == 0) printf("\n");
		printf("%.2X ", trama[i]);
	}
	printf("\n");
}

/**
 * Imprime una cadena en formato de MAC
 * @param mac [cadena a imprimir]
 */
void print_mac(u_char *mac) {
	int i;
	for (i = 0; i < 6; i++) {
		printf("%.2X", mac[i]);
		if (i < 5) printf(":");
	}
}

/**
 * Imprime una cadena en formato de IP
 * @param mac [cadena a imprimir]
 */
void print_ip(u_char *ip) {
	int i;
	for (i = 0; i < 4; i++) {
		printf("%d", ip[i]);
		if (i < 3) printf(".");
	}
}

/**
 * guarda una fraccion de una cadena en una cadena destino
 * @param opc    [formato a imprimir los bytes de la cadena]
 * @param t      [cadena fuente]
 * @param inicio [primer byte de la cadena donde se iniciara a imprimir]
 * @param final  [byte final a imprimir]
 */
void unpack_frame(u_char *trama, u_char *dest, int inicio, int final) { memcpy(dest, trama + inicio, final - inicio + 1); }

/**
 * Obtiene la informacion de la interfaz de Rede
 * @param  ds [descriptor de socket]
 * @return    [indice de la interfaz de red]
 */
int get_info(int ds, char *if_name, int *if_mtu, u_char *mac_source, u_char *ip_source, u_char *mask) {
	int index = 0;
	struct ifreq interfaz;

	// printf("\nInterfaz: ");
	// gets(if_name);
	strcpy(interfaz.ifr_name, "wlan0");

	/**
	 * Obtenemos el INDEX de la interfaz
	 */
	if (ioctl(ds, SIOCGIFINDEX, &interfaz) == -1)
		perror("\nerror al obtener Index\n");
	else
		index = interfaz.ifr_ifindex;

	/**
	 * Obtenemos el MTU de la interfaz
	 */
	if (ioctl(ds, SIOCGIFMTU, &interfaz) == -1)
		perror("\nerror al obtener MTU\n");
	else
		*if_mtu = interfaz.ifr_mtu;

	/**
	 * Obtenemos la MAC de la interfaz
	 */
	if (ioctl(ds, SIOCGIFHWADDR, &interfaz) == -1)
		perror("Error al obtener MAC\n");
	else {
		memcpy(mac_source, interfaz.ifr_hwaddr.sa_data, 6);
	}

	/**
	 * Obtenemos la IP de la interfaz
	 */
	if (ioctl(ds, SIOCGIFADDR, &interfaz) == -1)
		perror("Error al obtener IP\n");
	else {
		memcpy(ip_source, interfaz.ifr_hwaddr.sa_data + 2, 4);
	}

	/**
	 * Obtenemos la MASK de la interfaz
	 */
	if (ioctl(ds, SIOCGIFNETMASK, &interfaz) == -1)
		perror("Error al obtener Mask\n");
	else {
		memcpy(mask, interfaz.ifr_hwaddr.sa_data + 2, 4);
	}

	return index;
}

void get_gatewaymac(u_char *gatewaymac, u_char if_name) {
	FILE *f, *g;
	u_char ip[4];
	char command[50];
	/**
	 * Obtenemos la MAC de la puerta de enlace
	 */

	system(
		"route | grep default | grep -E -o -m1 "
		"\"(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]"
		"?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\" > gatewayip.txt");
	// system("iwlist wlan0 scan | grep -m 1 -o -E '([[:xdigit:]]{1,2}:){5}[[:xdigit:]]{1,2}' > gatewaymac.txt");

	if ((g = fopen("gatewayip.txt", "r")) == NULL) perror("fopen()");

	fscanf(g, "%d.%d.%d.%d", (int *)&(ip[0]), (int *)&(ip[1]), (int *)&(ip[2]), (int *)&(ip[3]));
	sprintf(command, "./arp wlan0 %d.%d.%d.%d > gatewaymac.txt", ip[0], ip[1], ip[2], ip[3]);
	system(command);

	if ((f = fopen("gatewaymac.txt", "r")) == NULL) perror("fopen()");

	fscanf(f, "%X:%X:%X:%X:%X:%X", (int *)&(gatewaymac[0]), (int *)&(gatewaymac[1]), (int *)&(gatewaymac[2]),
		   (int *)&(gatewaymac[3]), (int *)&(gatewaymac[4]), (int *)&(gatewaymac[5]));
	fclose(f);

	print_ip(ip);
	printf("\t");
	print_mac(gatewaymac);
	printf("\n");
}

void getip_url(u_char *url, u_char *ip) {
	struct sockaddr_in dest;
	struct hostent *hostaddr;

	dest.sin_family = AF_INET;
	hostaddr = gethostbyname((const char *)url);  // obtiene la ip de la entrada argv[1] con gethostbyname si es una URL
	memcpy(&dest.sin_addr, hostaddr->h_addr, hostaddr->h_length);
	memcpy(ip, &dest.sin_addr, hostaddr->h_length);
}

/**
 * Imprime la informacion de la interfaz de red
 */
void print_info(int index, int if_mtu, u_char *mac_source, u_char *ip_source, u_char *mask) {
	printf("\nMTU: %d", if_mtu);
	printf("\nMAC: ");
	print_mac(mac_source);
	printf("\nIP: ");
	print_ip(ip_source);
	printf("\nMask: ");
	print_ip(mask);
	printf("\nIndex: %d\n", index);
}

/**
 * Envia una trama por la interfaz mediante un socket
 * @param  ds    [Descriptor del socket]
 * @param  frame [trama a enviar]
 * @param  index [Indice de la interfaz de red]
 * @param  size  [Tamaño de la trama a enviar]
 * @return       [Regresa el tamaño de la trama ya enviada]
 */
int send_frame(int ds, u_char *frame, int index, int size) {
	/**
	 * Crear un struct sockaddr_ll
	 */
	struct sockaddr_ll nic;
	int tam;

	// Borramos el contenido de la estructura
	memset(&nic, 0x00, sizeof(nic));

	// Llenamos la estructura
	nic.sll_family = AF_PACKET;
	nic.sll_protocol = htons(ETH_P_ALL);
	nic.sll_ifindex = index;

	if ((tam = sendto(ds, frame, size, 0, (struct sockaddr *)&nic, sizeof(nic))) == -1) {
		perror("\nError al enviar trama\n");
	}
	return tam;
}

/**
 * Establece la cabecera Ethernet a una trama
 * @param paq        [trama a la cual se le estableceran los valores]
 */
void set_eth_header(u_char *paq, u_char *mac_target, u_char *mac_source, u_char *eth_type) {
	memcpy(paq + 0, mac_target, 6);
	memcpy(paq + 6, mac_source, 6);
	memcpy(paq + 12, eth_type, 2);
}

/**
 * Establece la cabecera ARP a una trama
 * @param paq        [trama a la cual se le estableceran los valores]
 */
void set_arp_header(u_char *paq, u_char *hw_type, u_char *proto_type, u_char *mac_len, u_char *ip_len, u_char *oper_code,
					u_char *mac_source, u_char *ip_source, u_char *mac_target, u_char *ip_target) {
	memcpy(paq + 14, hw_type, 2);
	memcpy(paq + 16, proto_type, 2);
	memcpy(paq + 18, mac_len, 1);
	memcpy(paq + 19, ip_len, 1);
	memcpy(paq + 20, oper_code, 2);
	memcpy(paq + 22, mac_source, 6);
	memcpy(paq + 28, ip_source, 4);
	memcpy(paq + 32, mac_target, 6);
	memcpy(paq + 38, ip_target, 4);
}

/**
 * Establece la cabecera IP a una trama
 * @param paq        [trama a la cual se le estableceran los valores]
 */
void set_ip_header(u_char *paq, u_char *version_ihl, u_char *type, u_char *len, u_char *id, u_char *flags_offset, u_char *ttl,
				   u_char *protocol, u_char *checksum, u_char *ip_source, u_char *ip_target) {
	memcpy(paq + 14, version_ihl, 1);
	memcpy(paq + 15, type, 1);
	memcpy(paq + 16, len, 2);
	memcpy(paq + 18, id, 2);
	memcpy(paq + 20, flags_offset, 2);
	memcpy(paq + 22, ttl, 1);
	memcpy(paq + 23, protocol, 1);
	memcpy(paq + 24, checksum, 2);
	memcpy(paq + 26, ip_source, 4);
	memcpy(paq + 30, ip_target, 4);
}

void set_icmp_header(u_char *paq, u_char *type, u_char *code, u_char *checksum, u_char *id, u_char *seq, u_char *data) {
	memcpy(paq + 34, type, 1);
	memcpy(paq + 35, code, 1);
	memcpy(paq + 36, checksum, 2);
	memcpy(paq + 38, id, 2);
	memcpy(paq + 40, seq, 2);
	memcpy(paq + 42, data, 32);
}

void set_udp_header(u_char *paq, u_char *po, u_char *pd, u_char *len, u_char *checksum) {
	memcpy(paq + 34, po, 1);
	memcpy(paq + 36, pd, 1);
	memcpy(paq + 38, len, 2);
	memcpy(paq + 40, checksum, 2);
}

/**
 * Crea una conexion a MySQL
 */
// int connect_mysql (MYSQL *con, char *server, char *user, char *password, char *db){
// 	con = mysql_init(NULL);
// 	/**
// 	 * Conectar a la base de datos
// 	 */
// 	if (!mysql_real_connect(con, server, user, password, db, 0, NULL, 0)){
// 		/**
// 		 * Si hay un error definir cual fue dicho error
// 		 */
// 		fprintf(stderr, "%s\n", mysql_error(con));
// 		return EXIT_FAILURE;
// 	}
// 	return EXIT_SUCCESS;
// }

/* Extra functions*/
char *itoa(int value) {
	int count, i, sign;
	char *ptr, *string, *temp;
	count = 0;
	if ((sign = value) < 0) { /* keep track and invert value */
		value = -value;
		count++; /* increment count */
	}
	temp = (char *)malloc(sizeof(int) + 2);
	if (temp == NULL) return (NULL);
	memset(temp, '\0', sizeof(int) + 2);
	string = (char *)malloc(sizeof(int) + 2);
	if (string == NULL) return (NULL);
	memset(string, '\0', sizeof(int) + 2);
	ptr = string;
	do {
		*temp++ = value % 10 + '0'; /* obtain modulus and or with '0' */
		count++;					/* increment count, track iterations*/
	} while ((value /= 10) > 0);
	if (sign < 0) /* add '-' when sign is negative */
		*temp++ = '-';
	*temp-- = '\0'; /* ensure null terminated and point */
	for (i = 0; i < count; i++, temp--, ptr++) memcpy(ptr, temp, sizeof(char));
	return (string);
}

void sscan_ip(u_char *dest, u_char *src) {
	sscanf(dest, "%d.%d.%d.%d", (int *)&src[0], (int *)&src[1], (int *)&src[2], (int *)&src[3]);
}

void sscan_mac(u_char *dest, u_char *src) {
	sscanf(dest, "%x:%x:%x:%x:%x:%x", (int *)&src[0], (int *)&src[1], (int *)&src[2], (int *)&src[3], (int *)&src[4],
		   (int *)&src[5]);
}
