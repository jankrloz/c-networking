#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "net.h"

int analyze_llc_frame (u_char *buf, int size){

	u_char mac_dest[6];
	u_char mac_src[6];
	u_char eth_type[2];

	u_char dlsap;
	u_char slsap;
	u_char control[2];
	u_char *info;
  
	memcpy(mac_dest, buf+0, 6);
	memcpy(&mac_src, buf+6, 6);
	memcpy(&eth_type, buf+12, 2);

	if (atoi(eth_type) > 0x00 && atoi(eth_type) <= 1500)
		exit(EXIT_FAILURE);

	memcpy(&dlsap, buf+14, 1);
	memcpy(&slsap, buf+15, 1);
	memcpy(&control, buf+16, 2);

	info = (u_char *)malloc(sizeof(u_char)*(size-18));
	memcpy(info, buf+18, size-18);

	printf("\n\n********************************************************************\n\n");

	printf("MAC Destino: ");
	print_mac(mac_dest);

	printf("\tMAC Fuente: ");
	print_mac(mac_src);

	printf ("\tEthertype: "); printf("%.2x %.2x", eth_type[0], eth_type[1]);

	printf("\n\nDLSAP: "); 

	if ((dlsap&0x80) == 0x80)
		printf("Grupo de Direccion destino: %.2x", dlsap&127);
	else
		printf("Destino Individual: %.2x", dlsap&127);

	printf("\nSLSAP: "); 

	if ((slsap&0x80) == 0x80)
		printf("Respuesta: %.2x", dlsap&127);
	else
		printf("Comando: %.2x", dlsap&127);

	printf ("\n\nCONTROL: ");

	if ((control[0]&0x80) != 0x80){
		printf("Información");
		printf("\nN(S): %d", control[0]&0x7f);
		printf("\nN(S): %d", control[1]&0x7f);
		printf("\nP/F: %d", (control[0]&0x80)>>7);
	}
	else{
		if ((control[0]&0x40) != 0x40){
			printf("Supervisión");
			printf("\nS: %d", (control[0]&0x30)>>4);
			printf("\nN(S): %d", control[1]&0x7f);
			printf("\nP/F: %d", (control[0]&0x80)>>7);
		}
		else{
			printf("No Numerada");
			printf("\nS: %d", (control[0]&0x30)>>4);
			printf("\nN(S): %d", control[1]&0x7f);
			printf("\nP/F: %d", (control[0]&0x80)>>7);
		}
	}

	return EXIT_SUCCESS;
}

int main(int argc, char const *argv[]){
	
	u_char *buffer;
	char aux[2];
	u_int tmp;
	FILE *f;
	int i, count;

	if((f = fopen("LLC.txt", "r")) == NULL)
		perror("fopen()");

	buffer = (u_char *)malloc(sizeof(u_char)*1514);
	
	while (!feof(f)){
		i = 0; count = 0;
		while(count < 4){ /** SI COUNT == 4, ES UNA NUEVA TRAMA (4 NEWLINES SEGUIDOS) **/
			
			aux[0] = fgetc(f);

			/** ESPACIO **/
			if (aux[0] == ' ')
				count = 0;

			/** HEXADECIMAL **/
			else if (isxdigit(aux[0])){
				aux[1] = fgetc(f);
				sscanf(aux, "%x", &tmp);
				//printf("%.2x ", tmp);
				memcpy(buffer+i, &tmp, 1);
				//printf ("%.2x ", buffer[i]);
				i++;
				count = 0;
			}

			/** NEWLINE **/
			else{
				//printf("%c", aux[0]);
				count++;
			}
		}
		//printf("%d\n", i);
		if (analyze_llc_frame(buffer, i) == EXIT_FAILURE)
				exit(EXIT_FAILURE);
	}
	fclose(f);

	return EXIT_SUCCESS;
}