#ifndef NET_H
#define NET_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

// #include "mysql/mysql.h" /* libreria que nos permite hacer el uso de las conexiones y consultas con MySQL */

/**
 * Nicknames
 */

typedef struct eth ETH;
typedef struct arp ARP;

/**
 * Prototipos de funciones
 */

/**
 * Guarda una direccion IP para enviar en una trama a partir de una cadena ingresada por el usuario
 * @param arg [Cadena ingresada por el usuario]
 * @param ip  [Cadena en donde se guardará la Direccion IP]
 */
void get_ip(const char *arg, u_char *ip);

/**
 * Obtiene la direccion IP de la red en la que se encuentra la terminal
 * @param ip   [IP fuente]
 * @param mask [Máscara de subred]
 * @param net  [Direccion destino]
 */
void get_net(u_char *ip, u_char *mask, u_char *net);

/**
 * Imprime en la salida estándar una cadena en formato de trama
 * @param tam   [tamaño de la trama a imprimir]
 * @param trama [cadena a imprimir]
 */
void print_frame(int tam, u_char *trama);

/**
 * Imprime una cadena en formato de MAC
 * @param mac [cadena a imprimir]
 */
void print_mac(u_char *mac);

/**
 * Imprime una cadena en formato de IP
 * @param mac [cadena a imprimir]
 */
void print_ip(u_char *ip);

/**
 * guarda una fraccion de una cadena en una cadena destino
 * @param opc    [formato a imprimir los bytes de la cadena]
 * @param t      [cadena fuente]
 * @param inicio [primer byte de la cadena donde se iniciara a imprimir]
 * @param final  [byte final a imprimir]
 */
void unpack_frame(u_char *trama, u_char *dest, int inicio, int final);

/**
 * Obtiene la informacion de la interfaz de Rede
 * @param  ds [descriptor de socket]
 * @return    [indice de la interfaz de red]
 */
int get_info(int ds, char *if_name, int *if_mtu, u_char *mac_source, u_char *ip_source, u_char *mask);

int get_ifindex(int ds, const char *if_name);

void get_gatewaymac(u_char *gatewaymac, u_char if_name);

void getip_url(u_char *url, u_char *ip);

/**
 * Imprime la informacion de la interfaz de red
 */
void print_info(int index, int if_mtu, u_char *mac_source, u_char *ip_source, u_char *mask);

/**
 * Envia una trama por la interfaz mediante un socket
 * @param  ds    [Descriptor del socket]
 * @param  frame [trama a enviar]
 * @param  index [Indice de la interfaz de red]
 * @param  size  [Tamaño de la trama a enviar]
 * @return       [Regresa el tamaño de la trama ya enviada]
 */
int send_frame(int ds, u_char *frame, int index, int size);

/**
 * Establece la cabecera Ethernet a una trama
 * @param paq           trama a la cual se le estableceran los valores
 */
void set_eth_header(u_char *paq, u_char *mac_target, u_char *mac_source, u_char *eth_type);

/**
 * Establece la cabecera ARP a una trama
 * @param paq        [trama a la cual se le estableceran los valores]

 */
void set_arp_header(u_char *paq, u_char *hw_type, u_char *proto_type, u_char *mac_len, u_char *ip_len, u_char *oper_code,
					u_char *mac_source, u_char *ip_source, u_char *mac_target, u_char *ip_target);

void set_ip_header(u_char *paq, u_char *version_ihl, u_char *type, u_char *len, u_char *id, u_char *flags_offset, u_char *ttl,
				   u_char *protocol, u_char *checksum, u_char *ip_source, u_char *ip_target);

void set_icmp_header(u_char *paq, u_char *type, u_char *code, u_char *checksum, u_char *id, u_char *seq, u_char *data);

void set_udp_header(u_char *paq, u_char *po, u_char *pd, u_char *len, u_char *checksum);

/**
 * Crea una conexion a MySQL
 */
// int connect_mysql (MYSQL *con, char *server, char *user, char *password, char *db);

char *itoa(int value);

void sscan_ip(u_char *dest, u_char *src);
void sscan_mac(u_char *dest, u_char *src);

#endif  // NET_H
