/**
 * 3_SERVER_ARP.C
 *
 * Juan Carlos Navarrete Gordillo
 * Escuela Superior de Cómputo, IPN
 *
 * Curso: Redes de Computadoras
 * Prof. Gilberto Sánchez Quintanilla
 *
 * Implementacion de Libreria de funciones útiles para el curso
 * Compilacion: gcc 2_scanner_arp.c -o scan -L. -lnet -lpthread $(mysql_config --cflags) $(mysql_config -–libs) -lmysqlclient
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netpacket/packet.h>
#include <net/if.h>
#include <net/ethernet.h> /* the l2 protocols */
#include <arpa/inet.h>
#include <fcntl.h>

#include <sys/ioctl.h>
#include <pthread.h>

#include "net.h"
#include "mysql/mysql.h"

char if_name[IF_NAMESIZE];
char query[256];
u_char terminals[255][2][6];
int if_mtu;
int if_index;
int id;
int packet_socket;

MYSQL *con;

/**
 * Tramas
 */
u_char sentframe[1514];
u_char recvframe[1514];

u_char mac_broadcast[6] = 				{0xff, 0xff, 0xff, 0xff, 0xff, 0xff};
u_char ip_source[4];
u_char mask[4];

u_char ip_scanned[4];
u_char mac_scanned[6];

/**
 * Protocolo Ethernet
 */
u_char mac_source[6];
u_char mac_target[6] = 					{0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
u_char eth_type[2] = 					{0x08,0x06};

/**
 * Protocolo ARP
 */
u_char hw_type[2] = 					{0x00, 0x01};
u_char proto_type[2] = 					{0x08, 0x00};
u_char mac_len[1] = 					{0x06};
u_char ip_len[1] = 						{0x04};
u_char oper_code_request[2] = 			{0x00, 0x01};
u_char oper_code_reply[2] = 			{0x00, 0x02};
u_char ip_target[4];

/**
 * ARP Gratuito
 */
u_char ip_offender[4];
u_char mac_offender[6];
u_char ip_defender[4];
u_char mac_defender[6];

int main (int argc, char *argv[]){
	int i = 0, j = 0;

	MYSQL_RES *res; /* variable que contendra el resultado de la consuta */
	MYSQL_ROW row; /* variable que contendra los campos por cada registro consultado */
	char *server = "localhost"; /*direccion del servidor 127.0.0.1, localhost o direccion ip */
	char *user = "root"; /*usuario para consultar la base de datos */
	char *password = "9212"; /* contraseña para el usuario en cuestion */
	char *database = "arp"; /*nombre de la base de datos a consultar */

	// /**
	//  * Checamos argumentos de ejecución
	//  */
	// if (argc != 2){
	// 	printf("ARP Request\nUso: sudo ./a.out [if_name]");
	// 	return EXIT_FAILURE;
	// }

	/**
	 * Creamos el socket crudo
	 */
	packet_socket = socket (AF_PACKET, SOCK_RAW, htons (ETH_P_ALL));

	/**
	 * Checamos errores
	 */
	if (packet_socket < 0)
		perror ("\nError al abrir socket\n");

	/**
	 * Establecemos banderas para socket no bloqueante
	 */
	if (fcntl(packet_socket , F_SETFL, fcntl(packet_socket, F_GETFL) | O_NONBLOCK) != 0)
		perror ("fcntl()");

	else{

		/**
		 * Obtenemos la informacion de la interfaz y de la terminal fuente
		 */
		if_index = get_info(packet_socket, if_name, &if_mtu, mac_source, ip_source, mask);
		//print_info(if_index, if_mtu, mac_source, ip_source, mask);

		/**
		 * Iniciamos la conexion a MySQL
		 */
		con = mysql_init(NULL);
		if (!mysql_real_connect(con, server, user, password, database, 0, NULL, 0)){
			/**
			 * Si hay un error definir cual fue dicho error
			 */
			fprintf(stderr, "%s\n", mysql_error(con));
			return EXIT_FAILURE;
		}

		/* enviar consulta SQL */
		if (mysql_query(con, "select * from Scan")){ 
			/* definicion de la consulta y el origen de la conexion */
			fprintf(stderr, "%s\n", mysql_error(con));
			exit(1);
		}	
		res = mysql_use_result(con);

		/**
		 * Guardamos las terminales autorizadas en un arreglo para su mejor manejo
		 */
		while ((row = mysql_fetch_row(res)) != NULL){
			memcpy(terminals[i][0], row[1], 4);
			memcpy(terminals[i][1], row[2], 6);
			i++;
		}

		printf("\nServidor ARP Iniciado . . .\n");

		while(1){

			/**
			 * Recibimos tramas de ARP Gratuito
			 */
			if(recvfrom(packet_socket, recvframe, 1514, 0, NULL, 0) == -1)
				;
			else if(!memcmp(recvframe+0, mac_broadcast, 6) && !memcmp(recvframe+12, eth_type, 2) && !memcmp(recvframe+20, oper_code_request, 2) && !memcmp(recvframe+32, mac_target, 6)){

				/**
				 * desempaquetamos las ip's de la solicitud ARP gratuito
				 */
				unpack_frame(recvframe, ip_offender, 28, 31);
				unpack_frame(recvframe, ip_target, 38, 41);

				/**
				 * Checamos si las ip's son iguales (ARP gratuito)
				 */
				if (!memcmp(ip_offender, ip_target, 4)){
					
					printf("\n\n\nTerminal infractora: ");
					unpack_frame(recvframe, mac_offender, 22, 37);
					print_mac(mac_offender);
					printf("\t\tPreguntando por: ");
					print_ip(ip_target);

					// print_frame(64, recvframe);

					j = 0;

					/**
					 * Buscamos si la ip destino esta en la lista de terminales autorizadas
					 * si asi es, enviamos su correspondiente respuesta
					 */
					while (j < i){
						// printf ("%d\n", j);
						
						if(!memcmp(ip_offender, terminals[j][0], 4)){
							memcpy(mac_source, terminals[j][1], 6);
							printf("\nTerminal NO Autorizada . . . Respuesta ARP");

							set_eth_header(sentframe, mac_offender, mac_source, eth_type);
							set_arp_header(sentframe, hw_type, proto_type, mac_len, ip_len, oper_code_reply, mac_source, ip_offender, mac_offender, ip_offender);

							send_frame(packet_socket, sentframe, if_index, 64);
							// print_frame(64, sentframe);

							printf("\nSolicitud ARP gratuito a todas las terminales");

							set_eth_header(sentframe, mac_broadcast, mac_source, eth_type);
							set_arp_header(sentframe, hw_type, proto_type, mac_len, ip_len, oper_code_request, mac_source, ip_offender, mac_target, ip_offender);

							send_frame(packet_socket, sentframe, if_index, 64);
							// print_frame(64, recvframe);

						}
						j++;
					}
				}
			}
		}
	}

	close (packet_socket);
	printf("\n");

	return 0;
}