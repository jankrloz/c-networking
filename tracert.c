////////////
// PING.C //
////////////

/**
 * Redes de computadoras
 * Prof. Gilberto Sanchez Quintanilla
 * por: Juan Carlos Navarrete
 * jankrloz.navarrete@gmail.com
 * ESCOM-IPN
 */

/**
 * Encabezado IP
 * IP origen, own IP
 * IP destino, IP ping
 *
 * Encabezado ICMP
 * tipo: 1
 * 		0 Respuesta de ECO
		3 Destino inalcanzable
		4 Flujo de origen
		5 Redirección
		8 Solicitud de ECO
		9 Anuncio de enrutador
		10 Solicitud de enrutador
		11 Tiempo de espera agotado
		12 Problema de parametros

 * codigo: 1
 * 		0
 * 		si es destino inalcanzable:
 * 		0. Red Inalcanzable
		1. Host Inalcanzable
		2. Protocolo Inalcanzable
		3. Puerto Inalcanzable
		4. Necesita Fragmentación y se especificó DF
		5. Error en ruta fuente
		6. Red de destino desconocida
		7. Host de destino desconocido
		8 Host de origen aislado
		9. Comunicación prohibida por el administrador con la red de destino
		10. Comunicación prohibida por el administrador con el host de destino
		11. Red Inaccesible por el tipo de servicio
		12. Host inalcanzable por el tipo de servicio
		13. Comunicación prohibida debido a un servidor de seguridad.

 * checksum: 2
 * id: 2
 * 		multiplo de 256
 * no seq: 2
 * 		multiplo de 256 y + 256 los demas
 * datos especificos: 4 o mas
 * 		"abcdefghijklmnopqrstuvwabcdefghi"
 */
#include "net.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <sys/time.h>
#include <sys/socket.h>
#include <netpacket/packet.h>
#include <net/if.h>
#include <net/ethernet.h> /* the l2 protocols */
#include <arpa/inet.h>
#include <fcntl.h>

#include <sys/ioctl.h>
#include <pthread.h>

char if_name[IF_NAMESIZE];
int if_mtu;
int if_index;

/**
 * Tramas
 */
u_char sentframe[1514];
u_char recvframe[1514];

u_char mac_broadcast[6] = 				{0xff,0xff,0xff,0xff,0xff,0xff};
u_char ip_source[4];
u_char mask[4];

/**
 * Protocolo Ethernet
 */
u_char mac_source[6];
u_char mac_target[6];
u_char eth_type[2] = 					{0x08,0x00};

/**
 * Protocolo IP
 */
u_char version_ihl[1] = 				{0x45};
u_char ip_type[1] = 					{0x00};
u_char len[2] = 						{0x00, 0x3c};
u_char ip_id[2] = 						{0x09, 0x0d};
u_char flags_offset[2] = 				{0x00, 0x00};
u_char ttl[1] = 						{0x80};
u_char protocol[1] = 					{0x01};
u_char ip_checksum[2] = 				{0x00, 0x00};
u_char ip_target[4] = 					{0x00, 0x00, 0x00, 0x00};

/**
 * Protocolo ICMP
 */
u_char type[1] = 						{0x08};
u_char code[1] = 						{0x00};
u_char icmp_checksum[2] = 				{0x00, 0x00};
u_char icmp_id[2] = 					{0x02, 0x00};
u_char icmp_seq[2] = 					{0x01, 0x00};
u_char data[32];

u_char echo_reply[1] = 					{0x00};
u_char ttlexpired[1] = 					{0x0b};
u_char unreachable[1] = 				{0x03};

u_short ip_cksum, icmp_cksum;

pthread_t ping_t, sleep_t;

u_char mac_gw[6];

/**
 * Definimos descriptor de socket
 */
int packet_socket;

int i, j, numbytes, success;
double secs, prom;
struct timeval t_ini, t_fin;

void *t_sleep (void *arg){
	usleep(1000000);
	pthread_cancel (ping_t);
	printf("%8c\t", '*');
	pthread_exit(0);
}

/* retorna "a - b" en segundos */
double timeval_diff(struct timeval *a, struct timeval *b){
  return
    (double)(a->tv_sec + (double)a->tv_usec/1000000) -
    (double)(b->tv_sec + (double)b->tv_usec/1000000);
}

/* Calculo del checksum */
u_short cksum (u_char *addr, int len){
	register int sum = 0;
	u_short answer = 0;
	u_short *wp;

	for (wp = (u_short*)addr; len > 1; wp++, len -= 2)
		sum += *wp;

	if (len == 1){
		*(u_char *)&answer = *(u_char *)wp;
		sum += answer;
	}

	sum = (sum >> 16) + (sum & 0xffff);

	sum += (sum >> 16);

	answer = ~sum;
	return answer;
}

void *t_ping (void *arg){

	gettimeofday(&t_ini, NULL);

	while (1){

		if((numbytes = recvfrom(packet_socket, recvframe, 1514, 0, NULL, 0)) == -1)
			;

		else if (!memcmp(recvframe+0, mac_source, 6) && !memcmp(recvframe+12, eth_type, 2)){
			if(!memcmp(recvframe+34, echo_reply, 1) && !memcmp (recvframe+38, icmp_id, 2) ){
				pthread_cancel(sleep_t);
				gettimeofday(&t_fin, NULL);
				secs = timeval_diff(&t_fin, &t_ini);

				printf("%8.4g ms\t",secs * 1000.0);

				prom += secs * 1000.0;
				success = 1;
				pthread_exit(0);
			}
			else if(!memcmp(recvframe+34, ttlexpired, 1) && !memcmp (recvframe+66, icmp_id, 2)){
				pthread_cancel(sleep_t);
				gettimeofday(&t_fin, NULL);
				secs = timeval_diff(&t_fin, &t_ini);

				printf("%8.4g ms\t",secs * 1000.0);

				prom += secs * 1000.0;
				pthread_exit(0);
			}
			else if (!memcmp(recvframe+34, unreachable, 1) && !memcmp (recvframe+66, icmp_id, 2)){
				pthread_cancel(sleep_t);
				printf("%8c\t", '*');
				pthread_exit(0);
			}
		}
		//printf("Recibida: \n");print_frame(84, recvframe);
	}
}

int main (int argc, char const *argv[])
{
	success = 0;
	int echo_ttl, sequence, n;
	/**
	 * Checamos argumentos de ejecución
	 */
	if (argc != 2){
		printf("ARP Request\nUso: sudo ./a.out [ip]");
		return EXIT_FAILURE;
	}

    /**
	 * Creamos el socket crudo
	 */
	packet_socket = socket (AF_PACKET, SOCK_RAW, htons (ETH_P_ALL));

	/**
	 * Checamos errores
	 */
	if (packet_socket < 0)
		perror ("\nError al abrir socket\n");

	/**
	 * Establecemos banderas para socket no bloqueante
	 */
	if (fcntl(packet_socket , F_SETFL, fcntl(packet_socket, F_GETFL) | O_NONBLOCK) != 0)
		perror ("fcntl()");

	get_gatewaymac(mac_target);

	/**
	 * Obtenemos la informacion de la interfaz y de la terminal fuente
	 */
	if_index = get_info(packet_socket, if_name, &if_mtu, mac_source, ip_source, mask);

	getip_url((u_char *)argv[1], ip_target);

	printf("\ntraceroute %s (", argv[1]); print_ip(ip_target); printf(") 30 Saltos MAX %d bytes. \n\n", 64);

	/**
	 * Establecemos las cabeceras necesarias a la trama
	 */
	set_eth_header(sentframe, mac_target, mac_source, eth_type);

	echo_ttl = 1;

	while (success != 1 && echo_ttl <= 30){

		memcpy(ttl, &echo_ttl, 1);

		set_ip_header (sentframe, version_ihl, ip_type, len, ip_id, flags_offset, ttl, protocol, ip_checksum, ip_source, ip_target);
		
		ip_cksum = cksum(sentframe+14, 20);

		memcpy(icmp_checksum, &ip_cksum, 2);
		memcpy(sentframe+24, icmp_checksum, 2);

		strcpy((char *)data, "juancarlosnavarretegordillo.1992");

		printf("%d\t", echo_ttl);

		for (n = 0; n < 3; n++){

			sequence = 0x01+n;

			memcpy(icmp_seq+0, &sequence, 1);
			memcpy(icmp_checksum, flags_offset, 2);

			set_icmp_header (sentframe, type, code, icmp_checksum, icmp_id, icmp_seq, data);

			icmp_cksum = cksum(sentframe+34, 40);
			
			memcpy(icmp_checksum, &icmp_cksum, 2);
			memcpy(sentframe+36, icmp_checksum, 2);

			send_frame(packet_socket, sentframe, if_index, 74);

			pthread_create(&ping_t, NULL, t_ping, NULL);
			pthread_create(&sleep_t, NULL, t_sleep, NULL);
			pthread_join(ping_t, NULL);
			pthread_join(sleep_t, NULL);
		}

		print_ip(recvframe+26);
		printf("\n");
		echo_ttl++;
	}

	printf("\nTraza Completa\n");

    return 0;
}
