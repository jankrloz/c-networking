/**
 * 2_SCANNER_ARP.C
 *
 * Juan Carlos Navarrete Gordillo
 * Escuela Superior de Cómputo, IPN
 *
 * Curso: Redes de Computadoras
 * Prof. Gilberto Sánchez Quintanilla
 *
 * Implementacion de Libreria de funciones útiles para el curso
 * Compilacion: gcc 1_arp_request.c -o req -L. -lnet
 */

/**
 * Librerias incluidas
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <sys/socket.h>
#include <netpacket/packet.h>
#include <net/if.h>
#include <net/ethernet.h> /* the l2 protocols */
#include <arpa/inet.h>
#include <fcntl.h>

#include <sys/ioctl.h>

#include "net.h"

char if_name[IF_NAMESIZE];
int if_mtu;
int if_index;

/**
 * Tramas
 */
u_char sentframe[1514];
u_char recvframe[1514];

u_char mac_broadcast[6] = 				{0xff,0xff,0xff,0xff,0xff,0xff};
u_char ip_source[4];
u_char mask[4];

/**
 * Protocolo Ethernet
 */
u_char mac_source[6];
u_char mac_target[6] = 					{0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
u_char eth_type[2] = 					{0x08,0x06};

/**
 * Protocolo ARP
 */
u_char hw_type[2] = 					{0x00, 0x01};
u_char proto_type[2] = 					{0x08, 0x00};
u_char mac_len[1] = 					{0x06};
u_char ip_len[1] = 						{0x04};
u_char oper_code_request[2] = 			{0x00, 0x01};
u_char oper_code_reply[2] = 			{0x00, 0x02};
u_char ip_target[4];

int main (int argc, char *argv[]){

	/**
	 * Definimos descriptor de socket
	 */
	int packet_socket;

	/**
	 * Checamos argumentos de ejecución
	 */
	if (argc != 2){
		printf("ARP Request\nUso: sudo ./a.out [ip]");
		return EXIT_FAILURE;
	}

	/**
	 * Creamos el socket crudo
	 */
	packet_socket = socket (AF_PACKET, SOCK_RAW, htons (ETH_P_ALL));

	/**
	 * Checamos errores
	 */
	if (packet_socket < 0)
		perror ("\nError al abrir socket\n");

	else{

		/**
		 * Obtenemos la informacion de la interfaz y de la terminal fuente
		 */
		if_index = get_info(packet_socket, if_name, &if_mtu, mac_source, ip_source, mask);
		print_info(if_index, if_mtu, mac_source, ip_source, mask);
		get_ip(argv[1], ip_target);

		/**
		 * Establecemos las cabeceras necesarias a la trama
		 */
		set_eth_header(sentframe, mac_broadcast, mac_source, eth_type);
		set_arp_header(sentframe, hw_type, proto_type, mac_len, ip_len, oper_code_request, mac_source, ip_source, mac_target, ip_target);

		printf("\nSolicitud ARP a terminal %s:\n", argv[1]);
		print_frame(48, sentframe);

		/**
		 * Enviamos la trama
		 */
		send_frame(packet_socket, sentframe, if_index, 64);

		/**
		 * Recibimos la trama respuesta
		 */
		while (1){
			if(recvfrom(packet_socket, recvframe, 1514, 0, NULL, 0) == -1)
				perror("Error al recibir trama\n");
			else if (!memcmp(recvframe+0, mac_source, 6) && !memcmp (recvframe+20, oper_code_reply, 2))//Filtro para que sea mi mac.
			    break;
		}
		printf("\nRespuesta ARP de la terminal %s:\n", argv[1]);
		print_frame(48, recvframe);
		printf ("\nla MAC destino es: ");

		/**
		 * Desencapsulamos la MAC de la terminal que respondio de la trama
		 */
		unpack_frame(recvframe, mac_target, 6, 11);
		print_mac(mac_target); printf("\n");
	}

	/**
	 * Cerramos el socket
	 */
	close (packet_socket);

	return 0;
}