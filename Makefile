run: arp req scan server infractor mysql llc ping tracert pathping udp

# Banderas
CFLAGS = -Wno-pointer-sign -Wno-deprecated-declarations -Wall

# Librerias

libnet.a: net.h net.o
	ar -rcs libnet.a net.o
net.o: net.c
	gcc -static -c net.c -o net.o -lmysqlclient $(mysql_config --cflags) $(mysql_config --libs)

# Codigos base

# ARP Request
req: 1_arp_request.c net.o libnet.a
	gcc 1_arp_request.c -o req -L. -lnet $(CFLAGS)

# ARP Request
arp: arpreq.c net.o libnet.a
	clang arpreq.c -o arp -L. -lnet $(CFLAGS)

# Scanner ARP
scan: 2_scanner_arp.c net.o libnet.a
	gcc 2_scanner_arp.c -o scan -L. -lnet -lpthread $(CFLAGS) $(mysql_config --cflags) $(mysql_config --libs) -lmysqlclient

# server ARP
server: 3_server_arp.c net.o libnet.a
	gcc 3_server_arp.c -o server -g -L. -lnet -lpthread $(CFLAGS) $(mysql_config --cflags) $(mysql_config --libs) -lmysqlclient

# infractor ARP gratuito
infractor: 3_infractor_arp.c net.o libnet.a
	gcc 3_infractor_arp.c -o infractor -L. -lnet -lpthread $(CFLAGS) $(mysql_config --cflags) $(mysql_config --libs) -lmysqlclient

# LLC Analyzer
llc: llc_analyzer.c net.o libnet.a
	gcc llc_analyzer.c -o llc -L. -lnet -lpthread $(CFLAGS) $(mysql_config --cflags) $(mysql_config --libs) -lmysqlclient

# Ping
ping: ping.c net.o libnet.a
	clang ping.c -o ping -L. -lnet -lpthread $(CFLAGS)

# tracert
tracert: tracert.c net.o libnet.a
	clang tracert.c -o tracert -L. -lnet -lpthread $(CFLAGS)

# pathping
pathping: pathping.c net.o libnet.a
	clang pathping.c -o pathping -L. -lnet -lpthread $(CFLAGS)

# udp
udp: udp.c net.o libnet.a
	clang udp.c -o udp -L. -lnet -lpthread $(CFLAGS)

#mysql
mysql: mysql.c
	gcc -o mysql $(mysql_config --cflags) mysql.c $(mysql_config --libs) -lmysqlclient

clean:
	rm *.o mysql req scan server infractor llc
