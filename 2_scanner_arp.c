/**
 * 2_SCANNER_ARP.C
 *
 * Juan Carlos Navarrete Gordillo
 * Escuela Superior de Cómputo, IPN
 *
 * Curso: Redes de Computadoras
 * Prof. Gilberto Sánchez Quintanilla
 *
 * Implementacion de Libreria de funciones útiles para el curso
 * Compilacion: gcc 2_scanner_arp.c -o scan -L. -lnet -lpthread $(mysql_config --cflags) $(mysql_config -–libs) -lmysqlclient
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netpacket/packet.h>
#include <net/if.h>
#include <net/ethernet.h> /* the l2 protocols */
#include <arpa/inet.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <pthread.h>

#include "net.h"
#include "mysql/mysql.h"

/**
 * Hilos de ejecución
 */
pthread_t wait_t, recv_t;

MYSQL *con;

char if_name[IF_NAMESIZE];
char query[256];
int if_mtu;
int if_index;
int id;
int packet_socket;

/**
 * Tramas
 */
u_char sentframe[1514];
u_char recvframe[1514];

u_char mac_broadcast[6] = {0xff, 0xff, 0xff, 0xff, 0xff, 0xff};
u_char ip_source[4];
u_char mask[4];

/**
 * Protocolo Ethernet
 */
u_char mac_source[6];
u_char mac_target[6] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
u_char eth_type[2] = {0x08, 0x06};

/**
 * Protocolo ARP
 */
u_char hw_type[2] = {0x00, 0x01};
u_char proto_type[2] = {0x08, 0x00};
u_char mac_len[1] = {0x06};
u_char ip_len[1] = {0x04};
u_char oper_code_request[2] = {0x00, 0x01};
u_char oper_code_reply[2] = {0x00, 0x02};
u_char ip_target[4];

/**
 * Prototipos de hilos
 */
void *thread_wait(void *arg);
void *thread_recv(void *a);

int main(int argc, char *argv[]) {
	int i;
	char *server = "localhost"; /*direccion del servidor 127.0.0.1, localhost o direccion ip */
	char *user = "root";		/*usuario para consultar la base de datos */
	char *password = "9212";	/* contraseña para el usuario en cuestion */
	char *database = NULL;		/*nombre de la base de datos a consultar */

	/**
	 * Creamos el socket crudo
	 */
	packet_socket = socket(AF_PACKET, SOCK_RAW, htons(ETH_P_ALL));

	/**
	 * Checamos errores
	 */
	if (packet_socket < 0) perror("\nError al abrir socket\n");

	/**
	 * Establecemos banderas para socket no bloqueante
	 */
	if (fcntl(packet_socket, F_SETFL, fcntl(packet_socket, F_GETFL) | O_NONBLOCK) != 0)
		perror("fcntl()");

	else {
		/**
		 * Obtenemos la informacion de la interfaz y de la terminal fuente
		 */
		if_index = get_info(packet_socket, if_name, &if_mtu, mac_source, ip_source, mask);
		print_info(if_index, if_mtu, mac_source, ip_source, mask);
		get_net(ip_source, mask, ip_target);

		/**
		 * Iniciamos la conexion a MySQL
		 */
		con = mysql_init(NULL);
		if (!mysql_real_connect(con, server, user, password, database, 0, NULL, 0)) {
			/**
			 * Si hay un error definir cual fue dicho error
			 */
			fprintf(stderr, "%s\n", mysql_error(con));
			return EXIT_FAILURE;
		}
		if (mysql_query(con, "CREATE DATABASE IF NOT EXISTS arp")) {
			fprintf(stderr, "%s\n", mysql_error(con));
			mysql_close(con);
		}
		if (mysql_query(con, "USE arp")) {
			fprintf(stderr, "%s\n", mysql_error(con));
			mysql_close(con);
		}
		if (mysql_query(con, "DROP TABLE IF EXISTS Scan")) {
			fprintf(stderr, "%s\n", mysql_error(con));
			mysql_close(con);
		}
		if (mysql_query(con, "CREATE TABLE Scan(id INT, ip BINARY(4), mac BINARY(6))")) {
			fprintf(stderr, "%s\n", mysql_error(con));
			mysql_close(con);
		}

		id = 0;

		printf("\nARP Scanning . . .\n");

		/**
		 * Establecemos las cabeceras necesarias a la trama
		 */
		set_eth_header(sentframe, mac_broadcast, mac_source, eth_type);
		set_arp_header(sentframe, hw_type, proto_type, mac_len, ip_len, oper_code_request, mac_source, ip_source, mac_target,
					   ip_target);

		i = 0;
		while (i < 255) {
			/**
			 * Asignamos la IP destino
			 */
			ip_target[3] = i;
			memcpy(sentframe + 38, ip_target, 4);

			/**
			 * Enviamos la trama
			 */
			send_frame(packet_socket, sentframe, if_index, 64);

			/**
			 * Creamos los hilos
			 */
			pthread_create(&recv_t, NULL, thread_recv, NULL);
			pthread_create(&wait_t, NULL, thread_wait, NULL);

			/**
			 * Esperamos la terminacion de la ejecucion de los hilos
			 */
			pthread_join(recv_t, NULL);
			pthread_join(wait_t, NULL);

			// printf(".");
			i++;
		}
	}

	/* se libera la variable res y se cierra la conexión */
	mysql_close(con);

	close(packet_socket);
	printf("\n");

	return 0;
}

/**
 * Hilo de ejecucion que dura un tiempo establecido y termina a otro hilo (Exclusion mutua)
 */
void *thread_wait(void *arg) {
	usleep(100000);
	pthread_cancel(recv_t);
	pthread_exit(0);
}

/**
 * Hilo de ejecución que recibe la trama especificada
 */
void *thread_recv(void *a) {
	while (1) {
		if (recvfrom(packet_socket, recvframe, 1514, 0, NULL, 0) == -1)
			;
		else if (!memcmp(recvframe + 0, mac_source, 6) && !memcmp(recvframe + 20, oper_code_reply, 2) &&
				 !memcmp(recvframe + 28, ip_target, 4)) {  // Filtro para que sea mi mac.
			/**
			 * Terminamos el hilo especificado
			 */
			pthread_cancel(wait_t);

			id++;

			/**
			 * Extraemos informacion de la trama recibida
			 */
			printf("\nIP: ");
			unpack_frame(recvframe, ip_target, 28, 31);
			print_ip(ip_target);
			printf("\t\tMAC: ");
			unpack_frame(recvframe, mac_target, 22, 27);
			print_mac(mac_target);

			sprintf(query, "INSERT INTO Scan VALUES(%d,X'%.2x%.2x%.2x%.2x',X'%.2x%.2x%.2x%.2x%.2x%.2x')", id, ip_target[0],
					ip_target[1], ip_target[2], ip_target[3], mac_target[0], mac_target[1], mac_target[2], mac_target[3],
					mac_target[4], mac_target[5]);

			printf("\n%s\n", query);

			if (mysql_query(con, query)) {
				fprintf(stderr, "%s\n", mysql_error(con));
				mysql_close(con);
			}

			/**
			 * Terminamos el hilo
			 */
			pthread_exit(0);
		}
	}
}
