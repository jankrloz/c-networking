/**
 * 2_SCANNER_ARP.C
 *
 * Juan Carlos Navarrete Gordillo
 * Escuela Superior de Cómputo, IPN
 *
 * Curso: Redes de Computadoras
 * Prof. Gilberto Sánchez Quintanilla
 *
 * Implementacion de Libreria de funciones útiles para el curso
 * Compilacion: gcc 2_scanner_arp.c -o scan -L. -lnet -lpthread
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netpacket/packet.h>
#include <net/if.h>
#include <net/ethernet.h> /* the l2 protocols */
#include <arpa/inet.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <pthread.h>

#include "net.h"
#include "mysql/mysql.h"

char if_name[IF_NAMESIZE];
char query[256];
int if_mtu;
int if_index;
int id;
int packet_socket;

MYSQL *con;

/**
 * Tramas
 */
u_char sentframe[1514];
u_char recvframe[1514];

u_char mac_broadcast[6] = 				{0xff, 0xff, 0xff, 0xff, 0xff, 0xff};
u_char mac_fake[6] = 				{0x11, 0x11, 0x11, 0x11, 0x11, 0x11};
u_char ip_source[4];
u_char ip_fake[4];
u_char mask[4];

/**
 * Protocolo Ethernet
 */
u_char mac_source[6];
u_char mac_target[6] = 					{0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
u_char eth_type[2] = 					{0x08,0x06};

/**
 * Protocolo ARP
 */
u_char hw_type[2] = 					{0x00, 0x01};
u_char proto_type[2] = 					{0x08, 0x00};
u_char mac_len[1] = 					{0x06};
u_char ip_len[1] = 						{0x04};
u_char oper_code_request[2] = 			{0x00, 0x01};
u_char oper_code_reply[2] = 			{0x00, 0x02};
u_char ip_target[4];

/**
 * ARP Gratuito
 */
u_char ip_offender[4];
u_char mac_offender[6];
u_char ip_defender[4];
u_char mac_defender[6];

int main (int argc, char *argv[]){

	char c;
	/**
	 * Checamos argumentos de ejecución
	 */
	if (argc != 2){
		printf("ARP Request\nUso: sudo ./a.out [ip a infringir]");
		return EXIT_FAILURE;
	}

	/**
	 * Creamos el socket crudo
	 */
	packet_socket = socket (AF_PACKET, SOCK_RAW, htons (ETH_P_ALL));

	/**
	 * Checamos errores
	 */
	if (packet_socket < 0)
		perror ("\nError al abrir socket\n");

	/**
	 * Establecemos banderas para socket no bloqueante
	 */
	if (fcntl(packet_socket , F_SETFL, fcntl(packet_socket, F_GETFL) | O_NONBLOCK) != 0)
		perror ("fcntl()");

	else{

		/**
		 * Obtenemos la informacion de la interfaz y de la terminal fuente
		 */
		if_index = get_info(packet_socket, if_name, &if_mtu, mac_source, ip_source, mask);
		print_info(if_index, if_mtu, mac_source, ip_source, mask);
		get_ip(argv[1], ip_target);

		printf("IP a infringir: ");
		print_ip(ip_target);

		/**
		 * Establecemos las cabeceras necesarias a la trama
		 */
		set_eth_header(sentframe, mac_broadcast, mac_fake, eth_type);
		set_arp_header(sentframe, hw_type, proto_type, mac_len, ip_len, oper_code_request, mac_fake, ip_target, mac_target, ip_target);

		while(1){
			/**
			 * Enviamos la trama
			 */
			send_frame(packet_socket, sentframe, if_index, 64);

			printf("\n\nEnviando Solicitud ARP Gratuito\n");
			// print_frame(64, sentframe);

			while (1){
				/**
				 * Recibimos la respuesta
				 */
				if(recvfrom(packet_socket, recvframe, 1514, 0, NULL, 0) == -1)
					; 
				else if(!memcmp(recvframe+0, mac_fake, 6) && !memcmp(recvframe+20, oper_code_reply, 2)){
					printf("\nRecibiendo Respuesta ARP Gratuito\n");
					break;
					// print_frame(64, recvframe);
				}
			}

			/**
			 * Esperamos un enter para volver a enviar y recibir tramas
			 */
			fflush(stdin);
			scanf("%c", &c);
		}
	}

	/**
	 * Cerramos el socket
	 */
	close (packet_socket);
	printf("\n");

	return 0;
}